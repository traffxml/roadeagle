Display town name with road name, if available
Show more road information (destinations or town and road name) for pl.gov.gddkia
Add support for height restrictions in lt.eismoinfo.restrictions
Fix delay handling in pl.gov.gddkia
Fix a bug in lt.eismoinfo.restrictions which resulted in missed feeds