/*
 * Copyright © 2017–2021 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.roadeagle.android.core;

import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;

import org.traffxml.roadeagle.R;
import org.traffxml.roadeagle.android.ui.MainActivity;
import org.traffxml.roadeagle.android.util.Const;
import org.traffxml.roadeagle.android.util.PermissionHelper;
import org.traffxml.source.android.MessageCache;
import org.traffxml.source.android.MessageListener;
import org.traffxml.traff.Traff;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.input.DataSource;
import org.traffxml.transport.android.AndroidTransport;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class TraffReceiver extends Service implements OnRequestPermissionsResultCallback {
	public static final String TAG = "TraffReceiver";

	private static final String CHANNEL_ID = "org.traffxml.roadeagle";

	/** The poll interval in seconds, if nothing else is configured explicitly */
	private static final int DEFAULT_POLL_INTERVAL = 600;

	/** Indicates that the service was started manually */
	public static final int FLAG_MANUAL = 0x1;

	/** Indicates that the service is being kept alive because the GUI is active */
	public static final int FLAG_GUI = 0x2;

	/** 
	 * Indicates that the service is being kept alive because at least one subscription is active.
	 * 
	 * Subscriptions will always start the service and keep it alive until the last subscription is cancelled,
	 * even if autoconnect is disabled.
	 */
	/*
	 * FIXME implement coverage areas for sources and only keep the service alive for subscriptions which
	 * overlap with the coverage area
	 */
	public static final int FLAG_SUBSCRIPTION = 0x8;

	/** Combination of all flags */
	public static final int FLAG_ALL = 0xFFFF;

	/** Combination of all flags which trigger autoconnect (all except manual and subscription) */
	public static final int FLAG_ALL_AUTO = FLAG_ALL ^ (FLAG_MANUAL | FLAG_SUBSCRIPTION);

	/** Maximum number of messages per feed */
	private static final int MAX_FEED_SIZE = 100;

	private static final int ONGOING_NOTIFICATION_ID = 0x27;

	/** The available data sources */
	private static Collection<DataSource> sources = new ArrayList<DataSource>();

	/** All currently configured source IDs */
	public static Set<String> sourceIds = new HashSet<String>();

	static {
		/* Harvest source IDs from properties */
		for (String key : RoadEagleApplication.receiverProperties.stringPropertyNames()) {
			Matcher matcher = Const.KEY_SOURCE_PATTERN.matcher(key);
			if (matcher.find())
				sourceIds.add(matcher.group(2));
		}
	}

	/** The TraFF message cache */
	private MessageCache cache = null;

	/** The base name for debug output files */
	private String debugSessionName = "null";

	/** Whether to log detailed information */
	private boolean debugVerbose;

	/**
	 * The intent which started the service.
	 */
	private Intent intent;

	/** Keepalive flags indicating which components may require the service to be running */
	private int keepaliveFlags = 0;

	/** Broadcast receiver which gets notified of events which might cause the service to stop */
	private BroadcastReceiver keepaliveFlagsReceiver = new BroadcastReceiver() {
		public void onReceive (Context context, Intent intent) {
			if (intent.getAction() == Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR) {
				/* update keepalive flags */
				keepaliveFlags &= ~intent.getIntExtra(Const.EXTRA_FLAGS, 0);
				if (keepaliveFlags == 0) {
					closeInput();
					stop();
				}
			}
		}
	};

	/** The number of messages currently in the cache */
	private int messageCount = 0;

	/** A {@link MessageListener} which updates the message count every time messages are added or deleted */
	private MessageListener messageListener;

	/** The notification builder for the notification shown while this service is running. */
	private Notification.Builder notificationBuilder;

	/** The timer to poll sources for updates */
	private Timer pollTimer = null;

	/** Whether the receiver is currently running */
	private boolean running = false;

	/** The unique ID with which the service was last started */
	private int startId;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate() " + this.toString());

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TraffReceiver.this);
		// TODO this should be moved to the RoadEagleApplication class
		preferences.registerOnSharedPreferenceChangeListener(new OnSharedPreferenceChangeListener() {
			@Override
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
				RoadEagleApplication.receiverProperties.setProperty(key, sharedPreferences.getAll().get(key).toString());
				// TODO reload sources if source config changed
			}
		});
		Map<String, ?> prefMap = preferences.getAll();
		for (String key : prefMap.keySet())
			RoadEagleApplication.receiverProperties.setProperty(key, prefMap.get(key).toString());

		/* message listener */
		messageListener = new MessageListener() {
			@Override
			public void onUpdateReceived(final Collection<TraffMessage> added) {
				messageCount = cache.size();
				updateNotification();

				// TODO revisit cancellation messages and message removal
				if ((added != null) && !added.isEmpty())
					broadcastMessages(added);
			}
		};
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if ((requestCode == Const.PERM_REQUEST_LOCATION_RECEIVER) && (grantResults.length > 0))
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				start();
			} else {
				Log.w(TAG, "ACCESS_COARSE_LOCATION permission not granted, aborting.");
				stopForeground(true);
				stopSelf();
			}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		this.intent = intent;
		this.startId = startId;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TraffReceiver.this);
		debugVerbose = preferences.getBoolean(Const.KEY_DEBUG_VERBOSE, false);

		if (intent.getAction() == Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_SET) {
			/* update keepalive flags */
			keepaliveFlags |= intent.getIntExtra(Const.EXTRA_FLAGS, 0);
			LocalBroadcastManager.getInstance(this).registerReceiver(keepaliveFlagsReceiver,
					new IntentFilter(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR));
		}

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {
			Log.i(TAG, "ACCESS_COARSE_LOCATION permission not granted, asking for it...");
			PermissionHelper.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					Const.PERM_REQUEST_LOCATION_RECEIVER,
					getString(R.string.notif_perm_title),
					getString(R.string.notif_perm_text),
					R.drawable.ic_permissions);
			makeForeground(); // FIXME test start without permissions
		} else
			/*
			 * the intent is a request to fetch data and we have permission; start service (it will stop when
			 * it is no longer needed)
			 */
			start();

		return START_REDELIVER_INTENT;
	}

	/**
	 * @brief Broadcasts a feed of messages, dividing it up into multiple feeds if necessary
	 * 
	 * @param messages Messages for the feed
	 */
	private void broadcastMessages(Collection<TraffMessage> messages) {
		/* convert input to an array list so we can perform subList operations */
		ArrayList<TraffMessage> messagesA = (messages instanceof ArrayList) ? ((ArrayList<TraffMessage>) messages) : new ArrayList<TraffMessage>(messages);

		/* list of lists */
		ArrayList<List<TraffMessage>> messageLists = new ArrayList<List<TraffMessage>>();

		for (int i = 0; i < messagesA.size(); i += MAX_FEED_SIZE)
			messageLists.add(messagesA.subList(i, Math.min(i + MAX_FEED_SIZE, messagesA.size())));

		for (List<TraffMessage> messageList : messageLists) {
			/* Broadcast the feed */
			Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_PUSH);
			outIntent.putExtra(AndroidTransport.EXTRA_PACKAGE, this.getPackageName());
			try {
				outIntent.putExtra(AndroidTransport.EXTRA_FEED, Traff.createFeed(messageList));
				sendBroadcast(outIntent, Manifest.permission.ACCESS_COARSE_LOCATION);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Performs cleanup tasks upon service shutdown.
	 * 
	 * This typically gets called upon clearing the last keepalive flag for the service, which
	 * indicates a state in which nobody is currently interested in traffic messages.
	 */
	private void closeInput() {
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TraffReceiver.this);
		boolean debugOutput = preferences.getBoolean(Const.KEY_DEBUG_OUTPUT, false);

		File dumpDir = TraffReceiver.this.getExternalFilesDir(null);
		if (debugOutput) {
			File outputFile = new File(dumpDir, debugSessionName + Const.FILE_EXTENSION_TRAFF);
			try {
				PrintWriter writer = new PrintWriter(outputFile);
				writer.println(Traff.createFeed(cache.query(null)));
				writer.close();
				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				Uri contentUri = Uri.fromFile(outputFile);
				mediaScanIntent.setData(contentUri);
				sendBroadcast(mediaScanIntent);
			} catch (Exception e) {
				if (debugVerbose)
					e.printStackTrace();
			}
		}
		((RoadEagleApplication)(this.getApplicationContext())).runMediaScanner();
	}

	/**
	 * @brief Returns the interval at which the source will be polled.
	 * 
	 * The poll interval is the interval set in the {@link Const#KEY_SOURCE_INTERVAL} property for
	 * the source, or {@link #DEFAULT_POLL_INTERVAL} if the former is not set or invalid. However,
	 * it is never less than {@link DataSource#getMinUpdateInterval()} for the source.
	 * 
	 * @param source The source
	 * 
	 * @return The update interval in seconds.
	 */
	private static int getPollInterval(DataSource source) {
		int interval = DEFAULT_POLL_INTERVAL;
		try {
			interval = Integer.valueOf(RoadEagleApplication.receiverProperties.getProperty(
					Const.getSourcePrefKey(source.getId(), Const.KEY_SOURCE_INTERVAL)));
		} catch (Exception e) {
			// NOP
		}
		interval = Math.max(interval, source.getMinUpdateInterval());
		return interval;
	}

	/**
	 * @brief Polls all sources for updates.
	 */
	private void poll() {
		for (DataSource source : sources) {
			if (!Boolean.parseBoolean(RoadEagleApplication.receiverProperties.getProperty(Const.getSourcePrefKey(source.getId(), Const.KEY_SOURCE_ENABLED), "false"))) {
				Log.d(TAG, String.format("Polling source %s", source.getId()));
				try {
					// TODO enforce limit on polling as per source.getMinUpdateInterval()
					Collection <TraffMessage> newMessages =
							source.poll(new ArrayList<TraffMessage>(cache.query(source.getId())), getPollInterval(source));
					if (newMessages != null)
						processMessages(newMessages);
				} catch (Exception e) {
					Log.w(TAG, String.format("Polling source %s failed with %s", source.getId(),
							e.getClass().getSimpleName()));
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @brief Processes a list of newly received TraFF messages.
	 * 
	 * @param newMessages The new messages
	 */
	private void processMessages(Collection<TraffMessage> newMessages) {
		/*
		 * For TMC, this method serves the additional purpose of tracking if we got any new messages from the
		 * current station. TODO what kind of processing do we need here, aside from committing to cache?
		 */
		try {
			cache.processUpdate(newMessages);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void start() {
		Log.d(TAG, String.format("Starting service %s for intent %s, keepaliveFlags %x", this.toString(), intent.getAction(), keepaliveFlags));

		makeForeground();

		if (keepaliveFlags == 0) {
			Log.w(TAG, "No keepaliveFlags set, aborting.");
			stopForeground(true);
			stopSelf();
			return;
		}

		if (!running) {
			if (pollTimer != null)
				pollTimer.cancel();
			pollTimer = new Timer("pollTimer", false);
		}

		StartupTask startupTask = new StartupTask();
		startupTask.execute(this.intent);
	}

	/**
	 * @brief Makes this service a foreground service.
	 */
	private void makeForeground() {
		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			CharSequence name = getString(R.string.channel_name);
			int importance = NotificationManager.IMPORTANCE_LOW;
			NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
			/*
			 * Register the channel with the system; you can't change the importance
			 * or other notification behaviors after this
			 */
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
			notificationBuilder = new Notification.Builder(this, CHANNEL_ID);
		} else
			notificationBuilder = new Notification.Builder(this);
		notificationBuilder.setContentTitle(String.format(getString(R.string.notification_title), messageCount));
		notificationBuilder.setContentText(getText(R.string.notification_text));
		notificationBuilder.setSmallIcon(R.drawable.ic_stat_notify);
		notificationBuilder.setContentIntent(pendingIntent);

		startForeground(ONGOING_NOTIFICATION_ID, notificationBuilder.getNotification());
	}

	/**
	 * @brief Stops the currently running service instance.
	 * 
	 * This unregisters the broadcast receiver and stops the service.
	 */
	private void stop() {
		if (!this.stopSelfResult(startId)) {
			Log.d(TAG, String.format("Ignoring stop request for service %s, keepaliveFlags %x: restart pending",
					this.toString(), keepaliveFlags));
			return;
		}
		Log.d(TAG, String.format("Stopping service %s, keepaliveFlags %x", this.toString(), keepaliveFlags));

		if (pollTimer != null)
			pollTimer.cancel();
		pollTimer = null;

		running = false;

		if (cache != null)
			cache.removeListener(messageListener);

		stopForeground(true);

		LocalBroadcastManager.getInstance(this).unregisterReceiver(keepaliveFlagsReceiver);

		/* Notify others that we’re stopping */
		Intent outIntent = new Intent(Const.ACTION_SERVICE_STOPPING);
		LocalBroadcastManager.getInstance(this).sendBroadcast(outIntent);
	}

	/**
	 * @brief Updates the notification.
	 */
	private synchronized void updateNotification() {
		notificationBuilder.setContentTitle(String.format(getString(R.string.notification_title), messageCount));
		notificationBuilder.setContentText(getString(R.string.notification_text));

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(ONGOING_NOTIFICATION_ID, notificationBuilder.getNotification());
	}


	private class StartupTask extends AsyncTask<Intent, Void, Void> {
		/** Whether the connection request was ignored. */
		boolean ignoreRequest = false;

		/** The intent with which we were started. */
		Intent intent;

		/** Text for toast alert to display to the user (if null, no toast will be displayed) */
		String toastText = null;

		/** Duration for toast alert (meaningful only if {@code toastText} is not null) */
		int toastLength = Toast.LENGTH_SHORT;

		/** Whether to suppress error messages */
		boolean silent;

		@Override
		protected Void doInBackground(Intent... params) {
			intent = params[0];

			if (cache == null) {
				try {
					cache = MessageCache.getInstance(TraffReceiver.this);
					cache.setAutoPurgeMessages(true);
					cache.setSubscriptionTimeout(Const.SUBSCRIPTION_TIMEOUT);
					messageCount = cache.size();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (intent.getAction() == Const.ACTION_INTERNAL_POLL)
				/* poll all sources for updates */
				poll();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (intent.getAction() == Const.ACTION_INTERNAL_POLL) {
				try {
					broadcastMessages(cache.query(null));
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				/* if we were not started for just a poll, mark ourselves as running and initialize */
				if (running)
					return;
				else
					running = true;

				for (String id : sourceIds) {
					String driver = RoadEagleApplication.receiverProperties.getProperty(Const.getSourcePrefKey(id, Const.KEY_SOURCE_DRIVER));
					if (driver == null) {
						Log.w(TAG, String.format("No driver specified for source %s, skipping", id));
						continue;
					}
					DataSource source;
					try {
						Class<?> clazz = Class.forName(driver);
						Class<? extends DataSource> subclazz = clazz.asSubclass(DataSource.class);
						Constructor<? extends DataSource> constr = subclazz.getConstructor(String.class, String.class, Properties.class);
						source = constr.newInstance(id,
								RoadEagleApplication.receiverProperties.getProperty(Const.getSourcePrefKey(id, Const.KEY_SOURCE_URL)),
								RoadEagleApplication.receiverProperties);
					} catch (Exception e) {
						Log.w(TAG, String.format("Could not load driver class %s for source %s, skipping\n", driver, id), e);
						continue;
					}
					sources.add(source);

					/*
					 * Schedule poll tasks:
					 * Delay is the source’s minimum update interval or the configured poll interval,
					 * whichever is longer
					 * Next poll is last poll time plus interval, or immediately if the source has not been
					 * polled for more than the interval (or not at all)
					 */
					/*
					 * FIXME
					 * last update is hardly ever used: if the source has never been polled, it will get
					 * polled immediately, then periodically after the interval elapses. If we’re unlucky,
					 * that will be just before the source updates and our data is always just below one
					 * interval old. A solution would be to poll once, then reschedule (either shortening or
					 * extending one interval).
					 * If the last update was more than one interval ago, the next one will be scheduled
					 * immediately as well.
					 */
					int interval = getPollInterval(source);
					Date first = source.getLastUpdate();
					if (first != null)
						first = new Date(Instant.ofEpochMilli(first.getTime()).plusSeconds(interval).toEpochMilli());
					if ((first == null) || first.before(new Date()))
						first = new Date();
					Log.d(TAG, String.format("Scheduling poll timer task for source %s, interval %d s", source.getId(), interval));
					pollTimer.schedule(new PollTimerTask(source), first, interval * 1000);
				}
			}

			if (ignoreRequest) {
				/* 
				 * start request was ignored (e.g. because the requested device is already connected), exit
				 */
				return;
			}

			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ROOT);
			fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			debugSessionName = fmt.format(new Date(System.currentTimeMillis()));
			File dumpDir = TraffReceiver.this.getExternalFilesDir(null);

			/* stop if we are no longer needed */
			if (keepaliveFlags == 0) {
				stop();
				return;
			}

			if (toastText != null)
				Toast.makeText(TraffReceiver.this, toastText, toastLength).show();

			cache.addListener(messageListener);

			/* Notify others that we’ve started */
			Intent outIntent = new Intent(Const.ACTION_SERVICE_STARTING);
			LocalBroadcastManager.getInstance(TraffReceiver.this).sendBroadcast(outIntent);
		}
	}

	private class PollTimerTask extends TimerTask {
		DataSource source;

		private PollTimerTask(DataSource source) {
			this.source = source;
		}

		@Override
		public void run() {
			if (!Boolean.parseBoolean(RoadEagleApplication.receiverProperties.getProperty(Const.getSourcePrefKey(source.getId(), Const.KEY_SOURCE_ENABLED), "false")))
				return;

			/* poll source */
			Log.d(TAG, String.format("Polling source %s" , source.getId()));
			try {
				Collection <TraffMessage> newMessages = source.poll(new ArrayList<TraffMessage>(cache.query(source.getId())), getPollInterval(source));
				if (newMessages != null) {
					Log.d(TAG, String.format("Source %s returned %d messages",
							source.getId(), newMessages.size()));
					processMessages(newMessages);
				} else
					Log.d(TAG, String.format("Source %s did not return any messages", source.getId()));
			} catch (Exception e) {
				Log.w(TAG, String.format("Polling source %s failed with %s", source.getId(),
						e.getClass().getSimpleName()));
				e.printStackTrace();
			}
			((RoadEagleApplication)(TraffReceiver.this.getApplicationContext())).runMediaScanner();
		}

	}
}
