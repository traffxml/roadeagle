/*
 * Copyright © 2017–2020 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.roadeagle.android.core;

import org.traffxml.roadeagle.android.util.Const;
import org.traffxml.source.android.DefaultSubscriptionListener;
import org.traffxml.source.android.MessageCache;
import org.traffxml.source.android.SubscriptionListener;
import org.traffxml.traff.Version;
import org.traffxml.traff.subscription.Capabilities;
import org.traffxml.traff.subscription.FilterList;
import org.traffxml.traff.subscription.Subscription;
import org.traffxml.transport.android.AndroidTransport;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Manages subscription requests and responds to subscriptionless poll requests from other applications.
 */
public class SubscriptionManager extends BroadcastReceiver {
	public static final String TAG = "SubscriptionManager";

	/**
	 * The authority for the content provider, without the package name.
	 * 
	 * Content provider URLs take the form {@code content://org.traffxml.roadeagle.MessageProvider/712aff1c}.
	 * The scheme is always {@code content}. The authority consists of the package name, which is read at run
	 * time, and the authority suffix appended to it. The path is the subscription ID.
	 */
	static final String AUTHORITY_SUFFIX = ".MessageProvider";

	static final int MIN_VERSION = Version.V0_7;
	static final int TARGET_VERSION = Version.V0_8;
	static final Capabilities CAPABILITIES = new Capabilities(MIN_VERSION, TARGET_VERSION);

	static SubscriptionListener subscriptionListener = null;

	/**
	 * @brief Returns the authority for the content provider.
	 * 
	 * @param context Any context belonging to the application
	 * @return The authority name
	 */
	String getAuthority(Context context) {
		return context.getPackageName() + AUTHORITY_SUFFIX;
	}

	/**
	 * @brief Returns the {@link SubscriptionListener} to notify when new messages become available for a subscription
	 * 
	 * The subscription listener returned will be an instance of {@link SubscriptionListener} registered to
	 * the application context. A new instance is created if none is available, else the previously created
	 * instance is returned.
	 * 
	 * @param context Any context belonging to the application
	 * @return The subscription listener
	 */
	SubscriptionListener getSubscriptionListener(Context context) {
		if (subscriptionListener == null) {
			subscriptionListener = new DefaultSubscriptionListener(getAuthority(context), context.getApplicationContext()) {
				@Override
				public void onLastSubscriptionExpired() {
					Log.d(TAG, String.format("Last subscription expired, notifying service"));
					/* this was the last subscription, update keepalive flags and notify service */
					Intent outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR, null,
							context, TraffReceiver.class);
					outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_SUBSCRIPTION);
					LocalBroadcastManager.getInstance(context).sendBroadcast(outIntent);
				}
			};
		}
		return subscriptionListener;
	}

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		FilterList filterList = null;
		String subscriptionId;
		Bundle resultExtras;
		Intent outIntent;
		switch (intent.getAction()) {
		case AndroidTransport.ACTION_TRAFF_GET_CAPABILITIES:
			Log.d(TAG, "GET_CAPABILITIES request");
			resultExtras = new Bundle();
			try {
				resultExtras.putString(AndroidTransport.EXTRA_PACKAGE, context.getPackageName());
				resultExtras.putString(AndroidTransport.EXTRA_CAPABILITIES, CAPABILITIES.toXml());
				this.setResult(AndroidTransport.RESULT_OK, null, resultExtras);
				Log.d(TAG, "Result: OK");
			} catch (Exception e) {
				e.printStackTrace();
				this.setResultCode(AndroidTransport.RESULT_INTERNAL_ERROR);
				Log.d(TAG, "Result: Internal error");
			}
			break;
		case AndroidTransport.ACTION_TRAFF_HEARTBEAT:
			subscriptionId = intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
			Log.d(TAG, String.format("HEARTBEAT request for %s", subscriptionId));
			if (subscriptionId == null) {
				this.setResultCode(AndroidTransport.RESULT_INVALID);
				Log.d(TAG, "Result: Invalid request");
			} else {
				try {
					MessageCache cache = MessageCache.getInstance(context);
					try {
						cache.touchSubscription(subscriptionId);
						this.setResult(AndroidTransport.RESULT_OK,
								null,
								null);
						Log.d(TAG, "Result: OK");
					} catch (IllegalArgumentException e) {
						this.setResultCode(AndroidTransport.RESULT_SUBSCRIPTION_UNKNOWN);
						Log.d(TAG, "Result: Subscription unknown");
					}
				} catch (Exception e) {
					this.setResultCode(AndroidTransport.RESULT_INTERNAL_ERROR);
					Log.d(TAG, "Result: Internal error");
				}
			}
			break;
		case AndroidTransport.ACTION_TRAFF_POLL:
			Log.d(TAG, "POLL request");
			/* Start the service with the internal poll intent */
			outIntent = new Intent(Const.ACTION_INTERNAL_POLL, null, context, TraffReceiver.class);
			outIntent.putExtra(Const.EXTRA_SILENT, true);
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
				context.startForegroundService(outIntent);
			else
				context.startService(outIntent);
			break;
		case AndroidTransport.ACTION_TRAFF_SUBSCRIBE:
			String packageName = intent.getStringExtra(AndroidTransport.EXTRA_PACKAGE);
			Log.d(TAG, String.format("SUBSCRIBE request from %s", packageName));
			try {
				filterList = FilterList.read(intent.getStringExtra(AndroidTransport.EXTRA_FILTER_LIST));
			} catch (Exception e) {
				// NOP
			}
			if ((packageName == null) || (filterList == null)) {
				this.setResultCode(AndroidTransport.RESULT_INVALID);
				Log.d(TAG, "Result: Invalid request");
			} else {
				try {
					MessageCache cache = MessageCache.getInstance(context);
					cache.addListener(getSubscriptionListener(context));
					Subscription subscription = cache.subscribe(packageName, filterList.getFilterListAsList());
					resultExtras = new Bundle();
					resultExtras.putString(AndroidTransport.EXTRA_PACKAGE, context.getPackageName());
					resultExtras.putString(AndroidTransport.EXTRA_SUBSCRIPTION_ID, subscription.id);
					this.setResult(AndroidTransport.RESULT_OK,
							(AndroidTransport.CONTENT_SCHEMA + "://" + getAuthority(context) + "/" + subscription.id),
							resultExtras);
					Log.d(TAG, "Result: OK");
					/* update keepalive flags and ensure the service is running */
					outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_SET, null,
							context, TraffReceiver.class);
					outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_SUBSCRIPTION);
					context.startService(outIntent);
				} catch (Exception e) {
					this.setResultCode(AndroidTransport.RESULT_INTERNAL_ERROR);
					Log.d(TAG, "Result: Internal error");
				}
			}
			break;
		case AndroidTransport.ACTION_TRAFF_SUBSCRIPTION_CHANGE:
			subscriptionId = intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
			Log.d(TAG, String.format("SUBSCRIPTION_CHANGE request for %s", subscriptionId));
			try {
				filterList = FilterList.read(intent.getStringExtra(AndroidTransport.EXTRA_FILTER_LIST));
			} catch (Exception e) {
				// NOP
			}
			if ((subscriptionId == null) || (filterList == null)) {
				this.setResultCode(AndroidTransport.RESULT_INVALID);
				Log.d(TAG, "Result: Invalid request");
			} else {
				try {
					MessageCache cache = MessageCache.getInstance(context);
					cache.addListener(getSubscriptionListener(context));
					try {
						cache.changeSubscription(subscriptionId, filterList.getFilterListAsList());
						this.setResult(AndroidTransport.RESULT_OK,
								(AndroidTransport.CONTENT_SCHEMA + "://" + getAuthority(context) + "/" + subscriptionId),
								null);
						Log.d(TAG, "Result: OK");
						/* update keepalive flags and ensure the service is running */
						outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_SET, null,
								context, TraffReceiver.class);
						outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_SUBSCRIPTION);
						context.startService(outIntent);
					} catch (IllegalArgumentException e) {
						this.setResultCode(AndroidTransport.RESULT_SUBSCRIPTION_UNKNOWN);
						Log.d(TAG, "Result: Subscription unknown");
					}
				} catch (Exception e) {
					this.setResultCode(AndroidTransport.RESULT_INTERNAL_ERROR);
					Log.d(TAG, "Result: Internal error");
				}
			}
			break;
		case AndroidTransport.ACTION_TRAFF_UNSUBSCRIBE:
			subscriptionId = intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
			Log.d(TAG, String.format("UNSUBSCRIBE request for %s", subscriptionId));
			if (subscriptionId == null) {
				this.setResultCode(AndroidTransport.RESULT_INVALID);
				Log.d(TAG, "Result: Invalid request");
			} else {
				try {
					MessageCache cache = MessageCache.getInstance(context);
					cache.unsubscribe(subscriptionId);
					this.setResult(AndroidTransport.RESULT_OK, null, null);
					Log.d(TAG, "Result: OK");
					if (!cache.hasSubscriptions()) {
						/* if this was the last subscription, update keepalive flags and notify service */
						outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR, null,
								context, TraffReceiver.class);
						outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_SUBSCRIPTION);
						LocalBroadcastManager.getInstance(context).sendBroadcast(outIntent);
					}
				} catch (Exception e) {
					this.setResultCode(AndroidTransport.RESULT_INTERNAL_ERROR);
					Log.d(TAG, "Result: Internal error");
				}
			}
			break;
			default:
				Log.d(TAG, String.format("Invalid request: %s", intent.getAction()));
		}
		((RoadEagleApplication)(context.getApplicationContext())).runMediaScanner();
	}
}
