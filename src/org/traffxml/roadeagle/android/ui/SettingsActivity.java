/*
 * Copyright © 2017–2021 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.roadeagle.android.ui;

import org.traffxml.roadeagle.android.core.RoadEagleApplication;
import org.traffxml.roadeagle.android.core.TraffReceiver;
import org.traffxml.roadeagle.android.util.Const;

import java.util.Set;
import java.util.TreeSet;

import org.traffxml.roadeagle.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;

public class SettingsActivity extends AppCompatActivity implements OnPreferenceClickListener, OnSharedPreferenceChangeListener {
	public static final String TAG = SettingsActivity.class.getSimpleName();

	public static final int REQUEST_CODE_PICK_RDS = 1;

	private SharedPreferences sharedPreferences;

	PreferenceCategory prefCatDebug;
	Preference prefDebugMirror;
	Preference prefDebugOutput;
	Preference prefDebugShow;
	Preference prefDebugVerbose;

	@Override
	protected void onPause() {
		sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected
	void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		// Show the Up button in the action bar.
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Display the fragment as the main content.
		getSupportFragmentManager().beginTransaction()
		.replace(android.R.id.content, new SettingsFragment())
		.commit();

		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if (preference == prefDebugShow) {
			Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
			editor.putBoolean(Const.KEY_DEBUG_SHOW, true);
			editor.apply();
			refreshDebugCategory();
			return true;
		} else
			return false;
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		// TODO schedule/unschedule sources as they get enabled/disabled
	}

	@Override
	protected void onStart() {
		super.onStart();

		SettingsFragment sf = (SettingsFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
		prefCatDebug = (PreferenceCategory) sf.findPreference(Const.PREF_CAT_DEBUG);
		prefDebugMirror = sf.findPreference(Const.KEY_DEBUG_MIRROR);
		prefDebugOutput = sf.findPreference(Const.KEY_DEBUG_OUTPUT);
		prefDebugShow = sf.findPreference(Const.KEY_DEBUG_SHOW);
		prefDebugShow.setOnPreferenceClickListener(this);
		prefDebugVerbose = sf.findPreference(Const.KEY_DEBUG_VERBOSE);

		refreshDebugCategory();

		PreferenceCategory prefCatSource = (PreferenceCategory) sf.findPreference(Const.PREF_CAT_SOURCE);
		/* sort source IDs */
		Set<String> sourceIds = new TreeSet<String>(TraffReceiver.sourceIds);
		for (String sourceId : sourceIds) {
			String key = Const.getSourcePrefKey(sourceId, Const.KEY_SOURCE_ENABLED);
			if (sf.findPreference(key) == null) {
				CheckBoxPreference sourcePref = new CheckBoxPreference(this);
				sourcePref.setKey(key);
				sourcePref.setTitle(sourceId);
				sourcePref.setSummary(RoadEagleApplication.receiverProperties.getProperty(Const.getSourcePrefKey(sourceId, Const.KEY_SOURCE_DESCRIPTION)));
				sourcePref.setLayoutResource(R.layout.preference_material);
				sourcePref.setWidgetLayoutResource(R.layout.preference_widget_checkbox);
				prefCatSource.addPreference(sourcePref);
			}
		}
	}

	/**
	 * @brief Shows or hides debug options depending on preference.
	 */
	private void refreshDebugCategory() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean debugShow = preferences.getBoolean(Const.KEY_DEBUG_SHOW, false);
		/*
		 * Due to an apparent bug in preference-v7, only set visibility if it has actually changed.
		 * Setting visibility to true on a visible item will cause it to show up twice.
		 * Setting visibility to false on a hidden item will result in an exception.
		 */
		if (debugShow) {
			if (prefDebugShow.isVisible())
				prefDebugShow.setVisible(false);
			if (!prefDebugOutput.isVisible())
				prefDebugOutput.setVisible(true);
			if (!prefDebugVerbose.isVisible())
				prefDebugVerbose.setVisible(true);
			if (!prefDebugMirror.isVisible())
				prefDebugMirror.setVisible(true);
		} else {
			if (!prefDebugShow.isVisible())
				prefDebugShow.setVisible(true);
			if (prefDebugOutput.isVisible())
				prefDebugOutput.setVisible(false);
			if (prefDebugVerbose.isVisible())
				prefDebugVerbose.setVisible(false);
			if (prefDebugMirror.isVisible())
				prefDebugMirror.setVisible(false);
		}
	}

	public static class SettingsFragment extends PreferenceFragmentCompat {
		@Override
		public void onCreatePreferences(Bundle savedInstanceState, String arg1) {
			//super.onCreatePreferences(savedInstanceState, arg1);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preferences);
		}

	}
}
