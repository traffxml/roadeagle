/*
 * Copyright © 2017–2021 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.roadeagle.android.ui;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.traffxml.roadeagle.R;
import org.traffxml.roadeagle.android.core.RoadEagleApplication;
import org.traffxml.roadeagle.android.core.TraffReceiver;
import org.traffxml.roadeagle.android.util.Const;
import org.traffxml.roadeagle.android.util.MessageHelper;
import org.traffxml.source.android.MessageCache;
import org.traffxml.source.android.MessageListener;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffMessage;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.Manifest;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	private static final String TAG = "MainActivity";

	/** The action bar for the activity */
	private ActionBar actionBar;

	/** The TMC message cache */
	private MessageCache cache = null;

	/** The menu item to connect to sources manually */
	private MenuItem connectSources = null;

	/** The menu item to disconnect from sources manually */
	private MenuItem disconnectSources = null;

	/** The listener which fires when a list item is clicked */
	private OnItemClickListener itemClickListener;

	/** The adapter through which the list view retrieves messages */
	private MessageAdapter messageAdapter;

	/** The Comparator to sort messages in the list */
	private Comparator<TraffMessage> messageComparator = new DefaultComparator();

	/** A {@link MessageListener} which updates {@code messageAdapter} every time messages are added or deleted */
	private MessageListener messageListener;

	/** Broadcast receiver which gets notified when the service stops */
	private BroadcastReceiver serviceStateReceiver = new BroadcastReceiver() {
		public void onReceive (Context context, Intent intent) {
			if (Const.ACTION_SERVICE_STOPPING.equals(intent.getAction()))
				setSourceState(false);
			else if (Const.ACTION_SERVICE_STARTING.equals(intent.getAction()))
				setSourceState(true);
		}
	};

	/** The UI element shown during startup */
	private ProgressBar startProgress;

	/** The UI element shown when there are no messages */
	private TextView emptyMessage;

	/** The UI element which displays the list of messages */
	private ListView messageView;

	/**
	 * @brief Disconnects from all sources.
	 */
	private void disconnect() {
		Intent outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR, null, this,
				TraffReceiver.class);
		outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_ALL);
		LocalBroadcastManager.getInstance(this).sendBroadcast(outIntent);
		setSourceState(false);
	}

	/**
	 * @brief Sets visibility of Sources menu items to reflect the source connection state.
	 * 
	 * @param connected Whether we are currently connected to the sources
	 */
	private void setSourceState(boolean connected) {
		if (connectSources != null)
			connectSources.setVisible(!connected);
		if (disconnectSources != null)
			disconnectSources.setVisible(connected);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startProgress = (ProgressBar) findViewById(R.id.startProgress);
		emptyMessage = (TextView) findViewById(R.id.emptyMessage);
		messageView = (ListView) this.findViewById(R.id.messageView);

		messageView.setVisibility(View.GONE);
		emptyMessage.setVisibility(View.GONE);
		startProgress.setVisibility(View.VISIBLE);

		actionBar = this.getSupportActionBar();

		StartupTask startupTask = new StartupTask();
		startupTask.execute();

		if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED)
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					Const.PERM_REQUEST_LOCATION_MAIN_RESUME);
	}

	@Override
	protected void onDestroy() {
		if (cache != null)
			cache.removeListener(messageListener);
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		/* update connection state */
		boolean isServiceRunning = false;
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
			if (TraffReceiver.class.getName().equals(service.service.getClassName()))
				isServiceRunning = true;
		setSourceState(isServiceRunning);
	}

	@Override
	protected void onStart() {
		super.onStart();
		//actionBar.setIcon(R.drawable.ic_signal_radio_null);
		actionBar.setSubtitle(null);
		actionBar.setDisplayShowHomeEnabled(true);

		boolean isServiceRunning = false;

		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
			if (TraffReceiver.class.getName().equals(service.service.getClassName()))
				isServiceRunning = true;

		if (isServiceRunning) {
			// TODO sendBroadcast(outIntent);
		} else {
			if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				ResumeTask resumeTask = new ResumeTask();
				resumeTask.execute();
			}
		}

		IntentFilter statusFilter = new IntentFilter();
		statusFilter.addAction(Const.ACTION_SERVICE_STARTING);
		statusFilter.addAction(Const.ACTION_SERVICE_STOPPING);
		LocalBroadcastManager.getInstance(this).registerReceiver(serviceStateReceiver, statusFilter);
	}

	@Override
	protected void onStop() {
		/* Tell the service the GUI has stopped */
		Intent outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR, null, this, TraffReceiver.class);
		outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_GUI);
		LocalBroadcastManager.getInstance(this).sendBroadcast(outIntent);

		LocalBroadcastManager.getInstance(this).unregisterReceiver(serviceStateReceiver);

		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean isServiceRunning = false;

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		connectSources = menu.findItem(R.id.action_connect_sources);
		disconnectSources = menu.findItem(R.id.action_disconnect_sources);

		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
			if (TraffReceiver.class.getName().equals(service.service.getClassName()))
				isServiceRunning = true;
		setSourceState(isServiceRunning);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		} else if (id == R.id.action_connect_sources) {
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				Intent outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_SET, null, this,
						TraffReceiver.class);
				outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_MANUAL);
				startService(outIntent);
				setSourceState(true);
			} else
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
						Const.PERM_REQUEST_LOCATION_MAIN_EXPLICIT);
			return true;
		} else if (id == R.id.action_disconnect_sources) {
			Intent outIntent = new Intent(Const.ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR, null, this,
					TraffReceiver.class);
			outIntent.putExtra(Const.EXTRA_FLAGS, TraffReceiver.FLAG_MANUAL);
			LocalBroadcastManager.getInstance(this).sendBroadcast(outIntent);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (((requestCode == Const.PERM_REQUEST_LOCATION_MAIN_EXPLICIT)
				|| (requestCode == Const.PERM_REQUEST_LOCATION_MAIN_RESUME))
				&& (grantResults.length > 0))
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// TODO connect to sources
				((RoadEagleApplication)(this.getApplicationContext())).runMediaScanner();
			} else {
				String message = getString(R.string.err_perm_location);
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				Log.w(TAG, "ACCESS_COARSE_LOCATION permission not granted, aborting connection attempt.");
			}
	}

	/**
	 * @brief The default comparator for sorting TMC messages.
	 * 
	 * This comparator compares events, using the following items of information in the order
	 * shown, until a difference is found:
	 * <ol>
	 * <li>Road class</li>
	 * <li>Road numbers</li>
	 * <li>if road numbers are present and equal, sequence of locations on the road
	 * <li>Area names</li>
	 * <li>Location IDs</li>
	 * </ol>
	 * 
	 * Road numbers and area names are sorted lexicographically. Null values are placed at the end,
	 * two null values are considered equal (causing the next items in the above list to be
	 * examined). Location IDs and extents are sorted numerically. 
	 */
	public static class DefaultComparator implements Comparator<TraffMessage> {
		private NumberStringComparator nsc = new NumberStringComparator();

		/**
		 * Returns the first and last point of the affected stretch for a TraFF message, not including
		 * auxiliary points.
		 */
		private static TraffLocation.Point[] getEndpointTuple(TraffMessage message) {
			if (message.location == null)
				return null;
			if (message.location.at != null)
				return new TraffLocation.Point[]{message.location.at, message.location.at};
			/* deal with malformed messages as well */
			if ((message.location.from == null) && (message.location.to == null))
				return null;
			if (message.location.from == null)
				return new TraffLocation.Point[]{message.location.to, message.location.to};
			if (message.location.to == null)
				return new TraffLocation.Point[]{message.location.from, message.location.from};
			/* default is to return first from, then to */
			return new TraffLocation.Point[]{message.location.from, message.location.to};
		}

		/**
		 * Returns whether the location refers to the backward direction of the road.
		 * 
		 * The direction is currently established by the ordering of the `distance` of the respective points.
		 * Future implementations may also consider junction numbers where only these are available.
		 */
		private static Boolean isBackward(TraffLocation location) {
			if ((location.from == null) && (location.to == null))
				return null;
			if (((location.from == null) || (location.to == null)) && (location.at == null) && (location.via == null))
				return null;
			TraffLocation.Point first = (location.from != null) ? location.from : (location.at != null) ? location.at : location.via;
			TraffLocation.Point last = (location.to != null) ? location.to : (location.at != null) ? location.at : location.via;
			if ((first == null) || (last == null) || (first.distance == null) || (last.distance == null))
				return null;
			return first.distance.floatValue() > last.distance.floatValue();
		}

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		@Override
		public int compare(TraffMessage lhs, TraffMessage rhs) {
			int res = 0;
			/* compare by road class */
			if ((lhs.location != null) && (lhs.location.roadClass != null)
					&& (rhs.location != null) && (rhs.location.roadClass != null))
				res = lhs.location.roadClass.compareTo(rhs.location.roadClass);
			else if ((lhs.location != null) && (lhs.location.roadClass != null))
				res = -1;
			else if ((rhs.location != null) && (rhs.location.roadClass != null))
				res = 1;
			if (res != 0)
				return res;

			/* compare by road numbers (if only one location has a road number, it is first) */
			String lrn = (lhs.location == null) ? null : lhs.location.roadRef;
			String rrn = (rhs.location == null) ? null : rhs.location.roadRef;
			if ((lrn != null) && (rrn != null)) {
				res = nsc.compare(lrn, rrn);
				if (res != 0)
					return res;
			} else if (lrn != null)
				return -1;
			else if (rrn != null)
				return 1;

			/* compare by country (if only one location has a country, it is first) */
			String lc = (lhs.location == null) ? null : lhs.location.country;
			String rc = (rhs.location == null) ? null : rhs.location.country;
			if ((lc != null) && (rc != null)) {
				res = lc.compareTo(rc);
				if (res != 0)
					return res;
			} else if (lc != null)
				return -1;
			else if (rc != null)
				return 1;

			// TODO compare by road name

			if ((lhs.location != null) && (rhs.location != null)) {
				Boolean lbk = null;
				Boolean rbk = null;
				if (lhs.location.directionality != TraffLocation.Directionality.BOTH_DIRECTIONS)
					lbk = isBackward(lhs.location);
				if (rhs.location.directionality != TraffLocation.Directionality.BOTH_DIRECTIONS)
					rbk = isBackward(lhs.location);

				/* compare by directionality and direction (first forward, then bidirectional, then backward) */
				if (Boolean.FALSE.equals(lbk)) {
					if (Boolean.TRUE.equals(rbk)
							|| (rhs.location.directionality == TraffLocation.Directionality.BOTH_DIRECTIONS))
						return -1;
				} else if (lhs.location.directionality == TraffLocation.Directionality.BOTH_DIRECTIONS) {
					if (Boolean.TRUE.equals(rbk))
						return -1;
					else if (Boolean.FALSE.equals(rbk))
						return 1;
				} else if (Boolean.TRUE.equals(lbk)) {
					if (Boolean.FALSE.equals(rbk)
							|| (rhs.location.directionality == TraffLocation.Directionality.BOTH_DIRECTIONS))
						return 1;
				}

				if (((lbk != null) && (rbk != null))
						|| ((lhs.location.directionality == TraffLocation.Directionality.BOTH_DIRECTIONS)
								&& (rhs.location.directionality == TraffLocation.Directionality.BOTH_DIRECTIONS))) {
					/* sort by kilometric points */
					TraffLocation.Point[] lep = getEndpointTuple(lhs);
					TraffLocation.Point[] rep = getEndpointTuple(rhs);

					if ((lep != null) && (rep != null)) {
						if (lhs.location.directionality == TraffLocation.Directionality.BOTH_DIRECTIONS
								|| !lbk.booleanValue()) {
							/* forward or bidirectional, sort kilometric points in ascending order */
							if ((lep[0].distance != null) && (rep[0].distance != null)) {
								if (lep[0].distance.floatValue() < rep[0].distance.floatValue())
									return -1;
								else if (lep[0].distance.floatValue() > rep[0].distance.floatValue())
									return 1;
								else if ((lep[0].distance.floatValue() == rep[0].distance.floatValue())
										&& (lep[1].distance != null) && (rep[1].distance != null)) {
									if (lep[1].distance.floatValue() < rep[1].distance.floatValue())
										return -1;
									else if (lep[1].distance.floatValue() > rep[1].distance.floatValue())
										return 1;
								}
							}
						} else {
							/* backward, sort kilometric points in descending order */
							if ((lep[0].distance != null) && (rep[0].distance != null)) {
								if (lep[0].distance.floatValue() > rep[0].distance.floatValue())
									return -1;
								else if (lep[0].distance.floatValue() < rep[0].distance.floatValue())
									return 1;
								else if ((lep[0].distance.floatValue() == rep[0].distance.floatValue())
										&& (lep[1].distance != null) && (rep[1].distance != null)) {
									if (lep[1].distance.floatValue() > rep[1].distance.floatValue())
										return -1;
									else if (lep[1].distance.floatValue() < rep[1].distance.floatValue())
										return 1;
								}
							}
						}
					}

					// TODO sort by junction numbers
				}
			}

			// TODO compare by coordinates
			
			return 0;
		}
	}

	/**
	 * @brief A comparator for strings containing numbers.
	 *
	 * To compare two strings, each string is broken down into numeric and non-numeric parts.
	 * Then both string are compared part by part. The following rules apply:
	 * <ul>
	 * <li>Whitespace bordering on a number is discarded</li>
	 * <li>Whitespace surrounded by string characters is treated as one string with the surrounding
	 * characters</li>
	 * <li>If one string has more parts than the other, the shorter string is padded with null
	 * parts.</li>
	 * <li>null is less than non-null.</li>
	 * <li>null equals null.</li>
	 * <li>A numeric part is less than a string part.</li>
	 * <li>Numeric parts are compared as integers.</li>
	 * <li>String parts are compared as strings.</li>
	 * </ul>
	 * 
	 * If one of the strings supplied is null or empty (but not the other), it is considered to be
	 * greater than the other, causing it to be inserted at the end.
	 */
	public static class NumberStringComparator implements Comparator<String> {
		private enum State { WHITESPACE, NUMERIC, ALPHA };

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		@Override
		public int compare(String lhs, String rhs) {
			int res = 0;
			if ((lhs == null) || lhs.isEmpty()) {
				if ((rhs == null) || rhs.isEmpty())
					return 0;
				else
					return 1;
			} else if ((rhs == null) || rhs.isEmpty())
				return -1;

			int i;
			List<Object> l = parse(lhs);
			List<Object> r = parse(rhs);
			for (i = 0; i < Math.min(l.size(), r.size()); i++) {
				if (l.get(i) instanceof Integer) {
					if (r.get(i) instanceof Integer)
						res = (Integer) l.get(i) - (Integer) r.get(i);
					else if (r.get(i) instanceof String)
						return -1;
				} else if (l.get(i) instanceof String) {
					if (r.get(i) instanceof Integer)
						return 1;
					else if (r.get(i) instanceof String)
						res = ((String) l.get(i)).compareTo((String) r.get(i));
				}
				if (res != 0)
					return res;
			}

			if (i < l.size())
				return 1;
			else if (i < r.size())
				return -1;
			else
				return 0;
		}

		/**
		 * @brief Parses a string into numeric and non-numeric components.
		 * @param string The string to parse
		 * @return A list of {@link String} and {@link Integer} objects.
		 */
		private List<Object> parse(String string) {
			List<Object> res = new LinkedList<Object>();
			State state = State.WHITESPACE;
			int i = 0;

			while (i < string.length()) {
				char c = string.charAt(i);
				if (c <= 0x20) {
					/* whitespace */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
						state = State.WHITESPACE;
					} else
						i++;
				} else if ((c >= '0') && (c <= '9')) {
					/* numeric */
					if (state == State.ALPHA) {
						res.add(string.substring(0, i).trim());
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.NUMERIC;
				} else {
					/* alpha */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.ALPHA;
				}
			}

			if (string.length() > 0) {
				if (state == State.NUMERIC)
					res.add(Integer.valueOf(string));
				else if (state == State.ALPHA)
					res.add(string.trim());
			}

			return res;
		}
	}

	/**
	 * @brief An adapter to display TMC messages in a ListView. 
	 */
	private class MessageAdapter extends ArrayAdapter<TraffMessage> {
		private TraffMessage selection = null;
		
		private MessageAdapter(Context context, int resource,
				int textViewResourceId) {
			super(context, resource, textViewResourceId);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			TextView country = (TextView) view.findViewById(R.id.country);
			TextView roadRef = (TextView) view.findViewById(R.id.roadRef);
			ImageView icon = (ImageView) view.findViewById(R.id.icon);
			TextView roadName = (TextView) view.findViewById(R.id.roadName);
			TextView locationName = (TextView) view.findViewById(R.id.locationName);
			TextView eventDescription = (TextView) view.findViewById(R.id.eventDescription);
			TextView source = (TextView) view.findViewById(R.id.source);
			TextView updated = (TextView) view.findViewById(R.id.updated);

			TraffMessage message = getItem(position);

			if (message == selection)
				view.setBackgroundColor(MainActivity.this.getResources().getColor(R.color.ripple_material_dark));
			else
				view.setBackgroundColor(MainActivity.this.getResources().getColor(R.color.background_material_dark));

			/* Country */
			String countryText = message.location.country;
			if (countryText != null) {
				country.setVisibility(View.VISIBLE);
				country.setText(countryText);
			} else
				country.setVisibility(View.INVISIBLE);

			/* roadRef: Road number */
			String roadRefText = message.location.roadRef;
			if (roadRefText != null) {
				roadRef.setVisibility(View.VISIBLE);
				roadRef.setText(roadRefText);
			} else {
				roadRef.setVisibility(View.INVISIBLE);
				roadRef.setText("");
			}
			TraffLocation.RoadClass rclass = message.location.roadClass;
			if (rclass == null)
				rclass = TraffLocation.RoadClass.OTHER;
			switch (rclass) {
			case MOTORWAY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.motorway));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_dark));
				break;
			case TRUNK:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.trunk));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_dark));
				break;
			case PRIMARY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.primary));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_dark));
				break;
			case SECONDARY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.secondary));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_light));
				break;
			case TERTIARY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.tertiary));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_light));
				break;
			default:
				/* use default color */
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.unclassified));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_light));
			}

			/* icon: Icon */
			TraffEvent.Class mclass = MessageHelper.getMessageClass(message);
			switch (mclass) {
			case ACTIVITY:
				icon.setImageResource(R.drawable.ic_activity);
				break;
			case AUDIO_BROADCAST:
				icon.setImageResource(R.drawable.ic_audio_broadcast);
				break;
			case AUTHORITY:
				icon.setImageResource(R.drawable.ic_authority);
				break;
			case CARPOOL:
				icon.setImageResource(R.drawable.ic_carpool);
				break;
			case CONGESTION:
				icon.setImageResource(R.drawable.ic_congestion);
				break;
			case CONSTRUCTION:
				icon.setImageResource(R.drawable.ic_construction);
				break;
			case DELAY:
				icon.setImageResource(R.drawable.ic_delay);
				break;
			case ENVIRONMENT:
				icon.setImageResource(R.drawable.ic_environment);
				break;
			case EQUIPMENT_STATUS:
				icon.setImageResource(R.drawable.ic_equipment_status);
				break;
			case HAZARD:
				icon.setImageResource(R.drawable.ic_hazard);
				break;
			case INCIDENT:
				icon.setImageResource(R.drawable.ic_incident);
				break;
			case PARKING:
				icon.setImageResource(R.drawable.ic_parking);
				break;
			case RESTRICTION:
				icon.setImageResource(R.drawable.ic_restriction);
				break;
			case SECURITY:
				icon.setImageResource(R.drawable.ic_security);
				break;
			case SERVICE:
				icon.setImageResource(R.drawable.ic_service);
				break;
			case TRANSPORT:
				icon.setImageResource(R.drawable.ic_transport);
				break;
			case TRAVEL_TIME:
				icon.setImageResource(R.drawable.ic_travel_time);
				break;
			case WEATHER:
				icon.setImageResource(R.drawable.ic_weather);
				break;
			default:
				icon.setImageResource(R.drawable.ic_special);
			}

			/* roadName: Road or location name */
			String direction = null;
			if (TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality)) {
				/* determine bearing between from and to; if exactly one of them is null, try at instead */
				TraffLocation.Point loc1 = message.location.from;
				TraffLocation.Point loc2 = message.location.to;
				if ((loc1 == null) && (loc2 != null))
					loc1 = message.location.at;
				else if ((loc1 != null) && (loc2 == null))
					loc2 = message.location.at;
				if ((loc1 != null) && (loc2 != null)) {
					double bearing = loc1.coordinates.bearingTo(loc2.coordinates);
					if ((bearing >= 330) || (bearing <= 30))
						direction = getContext().getString(R.string.direction_N);
					else if (bearing < 60)
						direction = getContext().getString(R.string.direction_NE);
					else if (bearing <= 120)
						direction = getContext().getString(R.string.direction_E);
					else if (bearing < 150)
						direction = getContext().getString(R.string.direction_SE);
					else if (bearing <= 210)
						direction = getContext().getString(R.string.direction_S);
					else if (bearing < 240)
						direction = getContext().getString(R.string.direction_SW);
					else if (bearing <= 300)
						direction = getContext().getString(R.string.direction_W);
					else
						direction = getContext().getString(R.string.direction_NW);
				}
			} else
				direction = getContext().getString(R.string.direction_both);
			if ((message.location.roadName != null) && !message.location.roadName.isEmpty()) {
				/* roadName specified: town, roadName, direction */
				roadName.setVisibility(View.VISIBLE);
				String prefix = (message.location.town == null) ? "" : (message.location.town + ", ");
				// TODO territory?
				if (direction != null)
					roadName.setText(prefix + message.location.roadName + ", " + direction);
				else
					roadName.setText(prefix + message.location.roadName);
			} else if ((message.location.origin != null) && !message.location.origin.isEmpty()
					&& (message.location.destination != null) && !message.location.destination.isEmpty()) {
				/* origin and destination specified: origin↔destination or origin→destination */
				roadName.setVisibility(View.VISIBLE);
				roadName.setText(message.location.origin
						+ (TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? " → " : " ↔ ")
						+ message.location.destination);
			} else if (((message.location.origin == null) || message.location.origin.isEmpty())
					&& ((message.location.destination == null) || message.location.destination.isEmpty())) {
				/* no origin and no destination: direction, if available */
				if (direction != null) {
					roadName.setVisibility(View.VISIBLE);
					roadName.setText(direction.substring(0, 1).toUpperCase() + direction.substring(1));
				} else
					roadName.setVisibility(View.GONE);
			} else if (TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality)) {
				/* unidirectional, either origin or destination: replace the missing one with the direction */
				roadName.setVisibility(View.VISIBLE);
				if ((message.location.origin != null) && !message.location.origin.isEmpty())
					roadName.setText(message.location.origin + " → " + ((direction != null) ? direction: ""));
				else
					roadName.setText(((direction != null) ? (direction.substring(0, 1).toUpperCase() + direction.substring(1)) : "") +
							" → " + message.location.destination);
			} else {
				/* bidirectional, only origin or only destination: no meaningful way to use the name */
				if (direction != null) {
					roadName.setVisibility(View.VISIBLE);
					roadName.setText(direction.substring(0, 1).toUpperCase() + direction.substring(1));
				} else
					roadName.setVisibility(View.GONE);
			}

			/* locationName: Detailed location ("at place", "between place1 and place2") */
			String locationNameText = MessageHelper.getDetailedLocationName(getContext(), message);
			if (locationNameText != null) {
				locationName.setVisibility(View.VISIBLE);
				locationName.setText(locationNameText);
			} else {
				locationName.setVisibility(View.GONE);
			}

			/* eventDescription */
			eventDescription.setText(MessageHelper.getEventText(getContext(), message));

			/* source */
			String serviceName = MessageHelper.getServiceName(message);
			source.setText(serviceName);

			/* updated */
			String updatedText = MessageHelper.formatTime(MainActivity.this, message.updateTime);
			updated.setText(updatedText.substring(0, 1).toUpperCase() + updatedText.substring(1));

			return view;
		}

		/**
		 * @brief Returns the selected message
		 * 
		 * @return The selected message, or null if none is selected
		 */
		protected TraffMessage getSelection() {
			return selection;
		}

		/**
		 * @brief Sets the selected message, unselecting any previously selected message.
		 * 
		 * Callers need to call {@link ArrayAdapter#notifyDataSetChanged()} separately after this
		 * method in order to ensure the update is displayed correctly in the associated list view.
		 * 
		 * @param message The message to select, or null to clear the selection
		 */
		protected void setSelection(TraffMessage message) {
			selection = message;
		}
	}

	private class ResumeTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			/* NOP */
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			((RoadEagleApplication)(MainActivity.this.getApplicationContext())).runMediaScanner();
		}
	}

	private class StartupTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			try {
				cache = MessageCache.getInstance(MainActivity.this);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			cache.setAutoPurgeMessages(true);
			cache.setSubscriptionTimeout(Const.SUBSCRIPTION_TIMEOUT);

			/* message adapter */
			messageAdapter = new MessageAdapter(MainActivity.this, R.layout.view_message_item, R.id.roadName);

			/* message listener */
			messageListener = new MessageListener() {
				@Override
				public void onUpdateReceived(final Collection<TraffMessage> added) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							TraffMessage selection = null;
							synchronized(messageAdapter) {
								if (messageAdapter.getSelection() != null)
									selection = messageAdapter.getSelection();

								/* begin "transaction", i.e. inhibit updates */
								messageAdapter.setNotifyOnChange(false);

								messageAdapter.setSelection(null);
								messageAdapter.clear();
								try {
									for (TraffMessage message: cache.query(null))
										if (!message.isCancellation) {
											messageAdapter.add(message);
											if ((selection != null) && (message.id.equals(selection.id)))
												selection = message;
										} else if ((selection != null) && (message.id.equals(selection.id)))
											selection = null;
									if (selection != null)
										messageAdapter.setSelection(selection);
								} catch (SQLException e) {
									e.printStackTrace();
								}

								if (messageAdapter.getCount() >= 1) {
									emptyMessage.setVisibility(View.GONE);
									messageView.setVisibility(View.VISIBLE);
									MainActivity.this.setTitle(String.format(getString(R.string.app_name_n),
											messageAdapter.getCount()));
									if (messageAdapter.getCount() > 1)
										messageAdapter.sort(messageComparator);
								} else {
									emptyMessage.setVisibility(View.VISIBLE);
									messageView.setVisibility(View.GONE);
									MainActivity.this.setTitle(getString(R.string.app_name));
								}

								/* "commit transaction", triggering an update */
								messageAdapter.notifyDataSetChanged();

								/* If a message is selected, keep it in view */
								if (selection != null) {
									int pos = messageAdapter.getPosition(selection);
									messageView.smoothScrollToPosition(pos);
								}
							}
						}
					});
				}
			};

			/* on click listener */
			itemClickListener = new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					synchronized(messageAdapter) {
						/* Select or unselect a message */
						TraffMessage message = messageAdapter.getItem(position);
						if (message == messageAdapter.getSelection())
							messageAdapter.setSelection(null);
						else
							messageAdapter.setSelection(message);
						messageAdapter.notifyDataSetChanged();
					}
				}
			};

			cache.addListener(messageListener);

			synchronized(messageAdapter) {
				messageAdapter.clear();
				try {
					for (TraffMessage message: cache.query(null))
						if (!message.isCancellation)
							messageAdapter.add(message);
					if (messageAdapter.getCount() > 1)
						messageAdapter.sort(messageComparator);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			messageView.setAdapter(messageAdapter);
			messageView.setOnItemClickListener(itemClickListener);

			startProgress.setVisibility(View.GONE);
			synchronized(messageAdapter) {
				if (messageAdapter.getCount() >= 1) {
					emptyMessage.setVisibility(View.GONE);
					messageView.setVisibility(View.VISIBLE);
					MainActivity.this.setTitle(String.format(getString(R.string.app_name_n),
							messageAdapter.getCount()));
				} else {
					emptyMessage.setVisibility(View.VISIBLE);
					messageView.setVisibility(View.GONE);
				}
			}
		}
	}
}
