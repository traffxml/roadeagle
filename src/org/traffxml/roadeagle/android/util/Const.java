/*
 * Copyright © 2017–2021 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.roadeagle.android.util;

import java.io.File;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.ContextWrapper;

/**
 * Constants used throughout the application
 */
public class Const {

	/**
	 * Internal intent to indicate that keepalive flags for the service have been cleared.
	 * 
	 * The flags to be changed need to be included as an extra, {@link #EXTRA_FLAGS}.
	 */
	public static final String ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR = "org.traffxml.roadeagle.KEEPALIVE_FLAGS_CLEAR";

	/**
	 * Internal intent to indicate that keepalive flags for the service have been set.
	 * 
	 * The flags to be changed need to be included as an extra, {@link #EXTRA_FLAGS}.
	 */
	public static final String ACTION_INTERNAL_KEEPALIVE_FLAGS_SET = "org.traffxml.roadeagle.KEEPALIVE_FLAGS_SET";

	/**
	 * Internal intent to poll the receiver service, starting it if necessary.
	 */
	public static final String ACTION_INTERNAL_POLL = "org.traffxml.roadeagle.POLL";

	/**
	 * Internal intent notifying interested parties that the TraFF receiver service is starting.
	 * 
	 * This intent will be sent as a broadcast.
	 */
	public static final String ACTION_SERVICE_STARTING = "org.traffxml.roadeagle.SERVICE_STARTING";

	/**
	 * Internal intent notifying interested parties that the TraFF receiver service is stopping.
	 * 
	 * This intent will be sent as a broadcast.
	 */
	public static final String ACTION_SERVICE_STOPPING = "org.traffxml.roadeagle.SERVICE_STOPPING";

	/**
	 * Extra for {@link #GPS_ENABLED_CHANGE} indicating whether GPS is active.
	 * 
	 * This is a Boolean extra.
	 */
	public static final String EXTRA_ENABLED = "enabled";

	/**
	 * Extra for {@link #ACTION_CANNED_DATA_REQUESTED} specifying the file to process.
	 * 
	 * This is a String extra.
	 */
	public static final String EXTRA_FILE = "file";

	/**
	 * Extra for {@link #ACTION_INTERNAL_KEEPALIVE_FLAGS_CLEAR} or {@link #ACTION_INTERNAL_KEEPALIVE_FLAGS_SET} indicating the flags to change.
	 * 
	 * This is an Integer extra.
	 */
	public static final String EXTRA_FLAGS = "flags";

	/**
	 * Extra for {@link #ACTION_BT_CONNECTION_REQUESTED} specifying that no feedback should be
	 * given to the user if the connection succeeded or failed. This is used for connections which
	 * were initiated automatically, not through the GUI.
	 * 
	 * This is a boolean extra.
	 */
	public static final String EXTRA_SILENT = "silent";

	/**
	 * File extension for TraFF dump files
	 */
	public static final String FILE_EXTENSION_TRAFF = ".xml";

	/**
	 * Intent indicating that GPS has been enabled or disabled
	 */
	public static final String GPS_ENABLED_CHANGE = "android.location.GPS_ENABLED_CHANGE";

	/**
	 * Permission request for location access when a connection is explicitly made in MainActivity
	 */
	public static final int PERM_REQUEST_LOCATION_MAIN_EXPLICIT = 1;

	/**
	 * Permission request for location access when MainActivity is resumed
	 */
	public static final int PERM_REQUEST_LOCATION_MAIN_RESUME = 2;

	/**
	 * Permission request for location access by TmcReceiver
	 */
	public static final int PERM_REQUEST_LOCATION_RECEIVER = 3;

	/**
	 * Permission request for location access by UsbHelper
	 */
	public static final int PERM_REQUEST_LOCATION_USB = 4;

	/**
	 * Preference key for the debug category.
	 */
	public static final String PREF_CAT_DEBUG = "pref_cat_debug";

	/**
	 * Preference key for the sources category.
	 */
	public static final String PREF_CAT_SOURCE = "pref_cat_source";

	/**
	 * Preference key for output debug setting.
	 */
	public static final String KEY_DEBUG_OUTPUT = "debug.output";

	/**
	 * Preference key to display debug settings.
	 */
	public static final String KEY_DEBUG_SHOW = "debug.show";

	/**
	 * Preference key for verbose logging.
	 */
	public static final String KEY_DEBUG_VERBOSE = "debug.verbose";

	/**
	 * Preference key for log mirroring.
	 */
	public static final String KEY_DEBUG_MIRROR = "debug.mirror";

	/**
	 * Preference key prefix for individual sources.
	 */
	public static final String KEY_SOURCE_PREFIX = "source";

	/**
	 * Preference key suffix for the source description string.
	 */
	public static final String KEY_SOURCE_DESCRIPTION = "description";

	/**
	 * Property key suffix for the setting which chooses the driver for the source.
	 */
	public static final String KEY_SOURCE_DRIVER = "driver";

	/**
	 * Preference key suffix for the setting which enables the source.
	 */
	public static final String KEY_SOURCE_ENABLED = "enabled";

	/**
	 * Property key suffix for the setting which determines the poll interval for the source.
	 */
	public static final String KEY_SOURCE_INTERVAL = "interval";

	/**
	 * Property key suffix for the setting which chooses the URL for the source.
	 */
	public static final String KEY_SOURCE_URL = "url";

	/**
	 * @brief Pattern which matches properties referring to a source.
	 * 
	 * This is used to “harvest” source IDs from the properties.
	 */
	public static final Pattern KEY_SOURCE_PATTERN = Pattern.compile("(source\\[)(.*?)(\\]\\..*)");

	/**
	 * Timeout for inactive subscriptions in seconds (3600 s = 1 hour).
	 */
	public static final int SUBSCRIPTION_TIMEOUT = 3600;

	/**
	 * @brief Returns the URL for the cache database.
	 * 
	 * @param context The calling context
	 * 
	 * @return A URL which can be passed directly to {@link MessageCache#setDatabaseUrl(String)}.
	 */
	public static String getCacheDbUrl(Context context) {
		ContextWrapper wrapper = new ContextWrapper(context);
		return "jdbc:hsqldb:file:" + (new File(wrapper.getFilesDir(), "messagecache")).getPath();
	}

	public static String getSourcePrefKey(String source, String pref) {
		return String.format("%s[%s].%s", KEY_SOURCE_PREFIX, source, pref);
	}
}
