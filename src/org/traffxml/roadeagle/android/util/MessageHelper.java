/*
 * Copyright © 2017–2021 Michael von Glasow.
 * 
 * This file is part of RoadEagle.
 *
 * RoadEagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RoadEagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RoadEagle.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.roadeagle.android.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Set;
import org.traffxml.traff.DimensionQuantifier;
import org.traffxml.traff.DurationQuantifier;
import org.traffxml.traff.FrequencyQuantifier;
import org.traffxml.traff.IntQuantifier;
import org.traffxml.traff.IntsQuantifier;
import org.traffxml.traff.PercentQuantifier;
import org.traffxml.traff.Quantifier;
import org.traffxml.traff.SpeedQuantifier;
import org.traffxml.traff.TemperatureQuantifier;
import org.traffxml.traff.TimeQuantifier;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.traff.WeightQuantifier;
import org.traffxml.roadeagle.R;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

/**
 * Methods for working with TMC messages.
 */
public class MessageHelper {
	private static final String TAG = "MessageHelper";

	/**
	 * If multiple event classes are present, the priority for the purposes of displaying an icon
	 */
	private static final List<TraffEvent.Class> CLASS_PRIORITY =
			new ArrayList<TraffEvent.Class>(Arrays.asList(new TraffEvent.Class[]{
					TraffEvent.Class.SECURITY,
					TraffEvent.Class.HAZARD,
					TraffEvent.Class.INCIDENT,
					TraffEvent.Class.WEATHER,
					TraffEvent.Class.EQUIPMENT_STATUS,
					TraffEvent.Class.CONSTRUCTION,
					TraffEvent.Class.DELAY,
					TraffEvent.Class.CONGESTION,
					TraffEvent.Class.RESTRICTION,
					TraffEvent.Class.TRAVEL_TIME,
					TraffEvent.Class.TRANSPORT,
					TraffEvent.Class.CARPOOL,
					TraffEvent.Class.PARKING,
					TraffEvent.Class.ENVIRONMENT,
					TraffEvent.Class.ACTIVITY,
					TraffEvent.Class.AUDIO_BROADCAST,
					TraffEvent.Class.SERVICE
			}));

	/**
	 * The default event comparator.
	 */
	private static final EventComparator EVENT_COMPARATOR = new EventComparator();

	/**
	 * @brief Returns a descriptive string for the detailed location of the message.
	 * 
	 * The detailed location typically refers to a stretch of road or a point on a road, which is
	 * the best approximation of the location of the event.
	 * 
	 * @param message
	 * @return
	 */
	public static String getDetailedLocationName(Context context, TraffMessage message) {
		String nameAt = getPointName(context, message.location.at);
		if (nameAt != null)
			return nameAt;

		String nameFrom = getPointName(context, message.location.from);
		String nameTo = getPointName(context, message.location.to);
		if (nameFrom != null) {
			if (nameTo == null) {
				return String.format("%s %s",
						nameFrom,
						TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? "→" : "↔");
			} else if (nameFrom.equals(nameTo))
				return nameFrom;
			else
				return String.format("%s %s %s",
						nameFrom,
						TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? "→" : "↔",
								nameTo);
		} else if (nameTo != null)
			return String.format("%s %s",
					TraffLocation.Directionality.ONE_DIRECTION.equals(message.location.directionality) ? "→" : "↔",
							nameTo);
		else {
			String nameVia = getPointName(context, message.location.via);
			return nameVia;
		}
	}

	/**
	 * @brief Returns a user-friendly string for the events in the message.
	 * 
	 * @param message
	 * @return
	 */
	public static String getEventText(Context context, TraffMessage message) {
		StringBuilder eventBuilder = new StringBuilder();
		List<TraffEvent> events = Arrays.asList(message.events);
		events.sort(EVENT_COMPARATOR);
		for (TraffEvent event : events) {
			if ((eventBuilder.length() > 0) && (eventBuilder.charAt(eventBuilder.length() - 1) != '?'))
				eventBuilder.append(". ");
			String eventText = getSingleEventText(context, event);
			eventText = eventText.substring(0, 1).toUpperCase() + eventText.substring(1);
			eventBuilder.append(eventText);

			for (TraffSupplementaryInfo si : event.supplementaryInfos)
				eventBuilder.append(", " + getSingleSiText(context, si));

			Integer length = event.length;
			if (length != null)
				try {
					eventBuilder.append(" ").append(getFormattedLength(context, length));
				} catch (IllegalFormatException e) {
					Log.w(TAG, "IllegalFormatException while parsing route length string, skipping");
					e.printStackTrace();
				}

			Integer speed = event.speed;
			if (speed != null)
				try {
					eventBuilder.append(", ").append(getFormattedSpeed(context, speed));
				} catch (IllegalFormatException e) {
					Log.w(TAG, "IllegalFormatException while parsing speed limit string, skipping");
					e.printStackTrace();
				}
		}

		if (eventBuilder.charAt(eventBuilder.length() - 1) != '?')
			eventBuilder.append(".");

		if ((message.startTime != null) || (message.endTime != null))
			eventBuilder.append(" ").append(getFormattedDuration(context, message)).append(".");

		return eventBuilder.toString();
	}

	/**
	 * @brief Returns the message class for a message.
	 * 
	 * The message class is determined based on the update classes of the constituent events of the
	 * message. Conflicts in determining the message class are resolved by applying the following
	 * order of precedence:
	 * <ul>
	 * <li>Security</li>
	 * <li>Warning</li>
	 * <li>Restriction</li>
	 * <li>Info</li>
	 * </ul>
	 * @param message
	 * @return
	 */
	public static TraffEvent.Class getMessageClass(TraffMessage message) {
		Set<TraffEvent.Class> classes = new HashSet<TraffEvent.Class>();
		for (TraffEvent event : message.events)
			classes.add(event.eventClass);

		for (TraffEvent.Class eClass : CLASS_PRIORITY)
			if (classes.contains(eClass))
				return eClass;
		/* fallback */
		return TraffEvent.Class.INVALID;
	}

	/**
	 * @brief Returns the service identifier of the message source.
	 * 
	 * @return The name, or null if anything goes wrong.
	 */
	public static String getServiceName(TraffMessage message) {
		try {
			return message.id.substring(0, message.id.indexOf(":"));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @brief Formats time as a relative string.
	 * 
	 * The output can take the following forms:
	 * <ul>
	 * <li>10:15 (today)</li>
	 * <li>Tomorrow 11:00 (yesterday or tomorrow)</li>
	 * <li>Monday 11:00 (up to a week in the past or future)</li>
	 * <li>December 10 (same year; no time given here)</li>
	 * <li>February 8, 2020 (outside the current year)</li>
	 * </ul>
	 * 
	 * The format reflects the quantifier {@code tmcTime}. The time ranges for these values overlap, but they also
	 * carry precision information.
	 * 
	 * @param context The context
	 * @param date The date to format
	 * @return A formatted date
	 */
	public static String formatTime(Context context, Date date) {
		if (date == null)
			return null;

		Calendar thenCal = new GregorianCalendar();
		thenCal.setTime(date);
		Calendar nowCal = new GregorianCalendar();

		if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)
				&& thenCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)
				&& thenCal.get(Calendar.DAY_OF_MONTH) == nowCal.get(Calendar.DAY_OF_MONTH))
			/* today (time only) */
			return DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
		else {
			Calendar thenDayCal = new GregorianCalendar(thenCal.get(Calendar.YEAR), thenCal.get(Calendar.MONTH),
					thenCal.get(Calendar.DAY_OF_MONTH));
			Calendar nowDayCal = new GregorianCalendar(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH),
					nowCal.get(Calendar.DAY_OF_MONTH));
			long dayDiff = (thenDayCal.getTimeInMillis() - nowDayCal.getTimeInMillis()) / 86400000;

			if (dayDiff == 1) {
				/* tomorrow (with time) */
				return String.format("%s, %s", context.getString(R.string.tomorrow),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (dayDiff == -1) {
				/* yesterday (with time) */
				return String.format("%s, %s", context.getString(R.string.yesterday),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (Math.abs(dayDiff) < 7) {
				/* within a week: day of week (with time) */
				return String.format("%s, %s", new SimpleDateFormat("EEEE").format(date),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)) {
				/* same year: month, day */
				return DateUtils.formatDateTime(context, date.getTime(), DateUtils.FORMAT_SHOW_DATE);
			} else
				/* different year: month, day, year */
				return DateFormat.getDateInstance(DateFormat.LONG).format(date);
		}
	}

	/**
	 * @brief Formats the event duration as a string.
	 * 
	 * @param context The context
	 * @param message The message
	 * @return
	 */
	private static String getFormattedDuration(Context context, TraffMessage message) {
		String startText = formatTime(context, message.startTime);
		String stopText = formatTime(context, message.endTime);
		if ((startText != null) && (stopText != null)) {
			String duration = String.format(context.getString(R.string.duration), startText, stopText);
			return duration.substring(0, 1).toUpperCase() + duration.substring(1);
		} else if (startText != null) {
			Date now = new Date();
			if (message.startTime.before(now))
				return String.format(context.getString(R.string.since), startText);
			else
				return String.format(context.getString(R.string.from), startText);
		} else if (stopText != null) {
			return String.format(context.getString(R.string.until), stopText);
		} else
			return "";
	}

	/**
	 * @brief Formats the route length as a string.
	 * 
	 * If the length argument exceeds 100, the "for more than 100 km" string will be returned.
	 * 
	 * @param context The context
	 * @param length The length of the route affected, in km
	 * 
	 * @return
	 */
	private static String getFormattedLength(Context context, int length) {
		if (length > 100)
			return context.getString(R.string.q_km_max);
		return context.getResources().getQuantityString(context.getResources().getIdentifier(
				"q_km", "plurals", context.getPackageName()), length, length);
	}

	/**
	 * @brief Formats an event quantifier as a string.
	 * 
	 * Quantifiers cannot be of the {@link IntQuantifier} or {@link IntsQuantifier} class, as these are
	 * processed directly by {@link #getSingleEventText(Context, Event)}.
	 * 
	 * @param context The context
	 * @param q The quantifier to format
	 * 
	 * @return
	 */
	private static String getFormattedQuantifier(Context context, Quantifier q) {
		int resid = 0;

		/* IntQuantifier and IntsQuantifier are handled outside this method. */
		if (q instanceof DimensionQuantifier) {
			resid = context.getResources().getIdentifier(
					"q_m", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((DimensionQuantifier) q).value,
					((DimensionQuantifier) q).value);
		} else if (q instanceof DurationQuantifier) {
			// TODO decide whether to use q_duration_min or q_duration_h
			resid = context.getResources().getIdentifier(
					"q_duration_min", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((DurationQuantifier) q).duration,
					((DurationQuantifier) q).duration);
		} else if (q instanceof FrequencyQuantifier) {
			// TODO decide whether to use q_freq_khz or q_freq_mhz
			resid = context.getResources().getIdentifier(
					"q_freq_mhz", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((FrequencyQuantifier) q).frequency,
					((FrequencyQuantifier) q).frequency);
		} else if (q instanceof PercentQuantifier) {
			resid = context.getResources().getIdentifier(
					"q_probability", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((PercentQuantifier) q).percent,
					((PercentQuantifier) q).percent);
		} else if (q instanceof SpeedQuantifier) {
			resid = context.getResources().getIdentifier(
					"q_km_h", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((SpeedQuantifier) q).speed,
					((SpeedQuantifier) q).speed);
		} else if (q instanceof TemperatureQuantifier) {
			resid = context.getResources().getIdentifier(
					"q_temperature_celsius", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((TemperatureQuantifier) q).degreesCelsius,
					((TemperatureQuantifier) q).degreesCelsius);
		} else if (q instanceof TimeQuantifier) {
			// TODO
			return "Sorry, not supported yet";
		} else if (q instanceof WeightQuantifier) {
			resid = context.getResources().getIdentifier(
					"q_t", "plurals", context.getPackageName());
			return context.getResources().getQuantityString(resid, (int) ((WeightQuantifier) q).weight,
					((WeightQuantifier) q).weight);
		} else {
			return "ILLEGAL";
		}
	}

	/**
	 * @brief Format the speed as a string.
	 * 
	 * Speed can refer to a speed limit or actual speed of moving traffic; this depends on the event type.
	 * 
	 * @param context The context
	 * @param speed The speed, in km/h
	 * 
	 * @return
	 */
	private static String getFormattedSpeed(Context context, int speed) {
		// TODO distinguish between speed limit and average speed (for display)
		return context.getResources().getQuantityString(context.getResources().getIdentifier(
				"q_maxspeed_km_h", "plurals", context.getPackageName()), speed, speed);
	}

	/**
	 * @brief Returns a descriptive string for a single point of the location.
	 * 
	 * The detailed location can be a junction name (with its reference number, if present), just the
	 * junction number or a kilometric point.
	 */
	private static String getPointName(Context context, TraffLocation.Point point) {
		if (point == null)
			return null;
		String res = point.junctionName;
		String junctionRef = point.junctionRef;
		String km = (point.distance != null) ? String.format(context.getString(R.string.distance), point.distance) : null;
		if ((junctionRef != null) && (!junctionRef.isEmpty())) {
			if (res == null)
				res = junctionRef;
			else
				res = res + " (" + junctionRef + ")";
		}
		if (km != null) {
			if (res == null)
				res = km;
			else if (junctionRef == null)
				res = res + " (" + km + ")";
			// TODO find a way to add junction name, ref and kmp
		}
		return res;
	}

	/**
	 * @brief Generates a formatted event description for a single event.
	 * 
	 * If the event has a quantifier, this method returns the description string for the event
	 * with the quantifier parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * @param context The context
	 * @param event The event
	 * @return The message
	 */
	private static String getSingleEventText(Context context, TraffEvent event) {
		return getTextWithQuantifier(context, "event_" + event.type.name().toLowerCase(), event.quantifier);
	}

	/**
	 * @brief Generates a formatted description for a single supplementary information item.
	 * 
	 * If the SI item has a quantifier, this method returns the description string for the item
	 * with the quantifier parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * @param context The context
	 * @param si The supplementary information item
	 * @return The message
	 */
	private static String getSingleSiText(Context context, TraffSupplementaryInfo si) {
		return getTextWithQuantifier(context, si.type.name().toLowerCase(), si.quantifier);
	}

	/**
	 * @brief Generates a formatted description for a single event or supplementary information item.
	 * 
	 * If the event or SI item has a quantifier, this method returns the description with the quantifier
	 * parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * The {@code type} argument is the type for the event or SI item. It can be obtained with the expression
	 * {@code event.type.name().toLowerCase()}. For an event, this string must be prefixed with
	 * {@code "event_"}.
	 * 
	 * @param context The context
	 * @param type The event or supplementary information type, see above
	 * @return The message
	 */
	private static String getTextWithQuantifier(Context context, String type, Quantifier quantifier) {
		String text = "";
		if (quantifier != null) {
			if (quantifier instanceof IntQuantifier) {
				int resid = context.getResources().getIdentifier(
						type + "_qn", "plurals", context.getPackageName());
				if (resid != 0)
					text = context.getResources().getQuantityString(resid,
							((IntQuantifier) quantifier).value,
							((IntQuantifier) quantifier).value);
				else {
					resid = context.getResources().getIdentifier(
							type + "_qn", "string", context.getPackageName());
					if (resid != 0)
						text = context.getString(resid,
								Integer.toString(((IntQuantifier) quantifier).value));
				}
			} else if (quantifier instanceof IntsQuantifier) {
				// TODO also handle partial quantifiers
			} else {
				int resid = context.getResources().getIdentifier(
						type + "_qn", "string", context.getPackageName());
				if (resid != 0) {
					String q = getFormattedQuantifier(context, quantifier);
					text = context.getString(resid, q);
				}
			}
		}
		if (text.isEmpty()) {
			text = context.getString(context.getResources().getIdentifier(
					type + "_q0", "string", context.getPackageName()));
		}
		return text;
	}

	private static class EventComparator implements Comparator<TraffEvent> {
		@Override
		public int compare(TraffEvent lhs, TraffEvent rhs) {
			// l < r ==> -1
			int res = 0;
			res = CLASS_PRIORITY.indexOf(lhs.eventClass) - CLASS_PRIORITY.indexOf(rhs.eventClass);
			if (res != 0)
				return res;
			res = lhs.type.compareTo(rhs.type);
			if (res != 0)
				return res;
			// TODO examine speed, quantifiers, probability, supplementary infos
			return res;
		}
	}
}
